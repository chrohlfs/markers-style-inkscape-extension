#!/usr/bin/env python
# coding=utf-8
#
#   Autrors:
#       Christian Rohlfs, https://gitlab.com/chrohlfs
#       Aaron Spike, aaron@ekips.org
#       Nicolas Dufour, nicoduf@yahoo.fr
#
#   Released under GNU GPL v2+,
#       see the 'LICENSE' file for more information.
#

import inkex

class MarkersStyle(inkex.EffectExtension):
    def add_arguments(self, pars):
        pars.add_argument("--fill_type", default="stroke", help="Marker fill color type")
        pars.add_argument("--stroke_type", default="stroke", help="Marker stroke color type")
        pars.add_argument("--fill_color", type=inkex.Color, default=inkex.Color(0x000000ff),
            help="Marker custom fill color")
        pars.add_argument("--stroke_color", type=inkex.Color, default=inkex.Color(0x000000ff),
            help="Marker custom stroke color")
        pars.add_argument("--fill_rule", default="evenodd", help="Marker fill rule")
        pars.add_argument("--stroke_width", type=float, default=1,
            help="Marker stroke width")
        pars.add_argument("--apply_to", default="any",
            help="Apply style to this type of markers")
        pars.add_argument("--overwrite", type=inkex.Boolean, default=False,
            help="Overwrite marker definition")

    def effect(self):
        marker_dict = {}
        original_marker_nodes = {}
        make_marker_copy = not self.options.overwrite

        marker_prop_names = ["marker", "marker-start", "marker-mid", "marker-end"]

        if self.options.apply_to != 'any':
            i = {'start':1, 'mid':2, 'end':3}[self.options.apply_to]
            marker_prop_names = [marker_prop_names[i]]

        for node in self.svg.selection.values():
            for prop in marker_prop_names:

                marker_node = node.style(prop)

                if marker_node is None or not isinstance(marker_node, inkex.Marker):
                    continue

                if make_marker_copy:
                    if marker_node in original_marker_nodes:
                        # we already have a copy
                        new_marker_node = original_marker_nodes[marker_node]
                    else:
                        # make a new one
                        marker_id = marker_node.get_id()
                        new_marker_node = marker_node.copy()
                        new_id = self.svg.get_unique_id(marker_id)
                        new_marker_node.set("id", new_id)
                        self.svg.defs.append(new_marker_node)
                else:
                    # overwrite existing marker node
                    new_marker_node = marker_node

                original_marker_nodes[marker_node] = new_marker_node

                # marker_dict struct:
                #   { marker_node: {node: {attr1, attr2}} }
                node_dict = marker_dict.get(new_marker_node, {})
                prop_set = node_dict.get(node, set())
                prop_set.add(prop)
                node_dict[node] = prop_set
                marker_dict[new_marker_node] = node_dict


        color_dict = {'stroke':'context-stroke', 'fill':'context-fill', 'none':'none'}

        for marker_node, node_dict in marker_dict.items():
            for child in marker_node:

                if self.options.fill_type == 'custom':
                    child.style.set_color(self.options.fill_color, 'fill')
                    child.style['fill-opacity'] = self.options.fill_color.alpha
                elif self.options.fill_type != 'dont':
                    child.style['fill'] = color_dict[self.options.fill_type]

                if self.options.stroke_type == 'custom':
                    child.style.set_color(self.options.stroke_color, 'stroke')
                    child.style['stroke-opacity'] = self.options.stroke_color.alpha
                elif self.options.stroke_type != 'dont':
                    child.style['stroke'] = color_dict[self.options.stroke_type]

                if self.options.fill_type != 'dont':
                    child.style['fill-rule'] = self.options.fill_rule
                if self.options.stroke_type != 'dont':
                    child.style['stroke-width'] = self.options.stroke_width

            if make_marker_copy:
                for node, prop_set in node_dict.items():
                    for prop in prop_set:
                        node.style[prop] = marker_node


if __name__ == "__main__":
    MarkersStyle().run()
