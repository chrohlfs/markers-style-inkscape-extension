# Markers style (Inkscape extension)

![tree](https://gitlab.com/chrohlfs/markers-style-inkscape-extension/-/raw/main/chrtree.png) Blurred christmas markers. 

## Description

This Inkscape extension allows you to change style of markers of selected objects. Fill and stroke color of a marker can be set to inherit from its object's fill or stroke, this is useful for custom made markers as their colors are static by default.

If installed, it should appear under `Extensions -> Stylesheet -> Markers style...`.

![screenshot](https://gitlab.com/chrohlfs/markers-style-inkscape-extension/-/raw/main/screenshot.png)

**Issues:** 

1. If `Overwrite marker definitions` is chosen then Inkscape will hide markers until file reopen. See  report [#3136](https://gitlab.com/inkscape/inkscape/-/issues/3136).
2. Groups in markers are not supperted. If you're planning to use multiple objects for a marker, then, for extension to work with such markers, you should just select your objects and convert them to marker (`Object -> Objects to Marker`) without grouping.

## Installation

Just copy `markers_style.inx` and `markers_style.py` to the Inkscape user extensions folder or its subfolder, restart the program and you're done. The extension should appear under `Extensions -> Stylesheet -> Markers style...`.

To find out the path to the user extensions folder open Inkscape and naviate to `Preferences -> System -> User extensions:`.

## License

Released under GNU GPL v2+, see the 'LICENSE' file for more information.

## Authors

Christian Rohlfs, Aaron Spike, Nicolas Dufour

This extension is the complete rewrite of the `Color markers` extension bundled with Inkscape to support `context-fill` and `context-stroke` values for a marker's `fill` and `stroke` CSS properties.
